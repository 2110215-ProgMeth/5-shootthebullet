package test;

import static org.junit.Assert.*;

import org.junit.Test;

import javafx.scene.input.KeyCode;
import lib.InputUtility;

public class TestInputUtility {

	@Test
	public void testKeyPressed() {
		boolean keyPressed;
		InputUtility.setKeyPressed(KeyCode.SPACE, true);
		keyPressed = InputUtility.getKeyPressed(KeyCode.SPACE);
		assertTrue(keyPressed);
		keyPressed = InputUtility.getKeyPressed(KeyCode.ENTER);
		assertFalse(keyPressed);

		InputUtility.setKeyPressed(KeyCode.SPACE, false);
		keyPressed = InputUtility.getKeyPressed(KeyCode.SPACE);
		assertFalse(keyPressed);
		keyPressed = InputUtility.getKeyPressed(KeyCode.ENTER);
		assertFalse(keyPressed);
	}

	@Test
	public void testKeyTriggered() {
		boolean keyTriggered;
		InputUtility.setKeyTriggered(KeyCode.SPACE, true);
		keyTriggered = InputUtility.getKeyTriggered(KeyCode.SPACE);
		assertTrue(keyTriggered);
		keyTriggered = InputUtility.getKeyTriggered(KeyCode.ENTER);
		assertFalse(keyTriggered);

		InputUtility.setKeyTriggered(KeyCode.SPACE, false);
		keyTriggered = InputUtility.getKeyTriggered(KeyCode.SPACE);
		assertFalse(keyTriggered);
		keyTriggered = InputUtility.getKeyTriggered(KeyCode.ENTER);
		assertFalse(keyTriggered);
	}

	@Test
	public void testPostUpdate() {
		InputUtility.setKeyTriggered(KeyCode.SPACE, true);
		InputUtility.setMouseLeftDown(true);
		InputUtility.setMouseRightDown(true);
		InputUtility.postUpdate();

		assertFalse(InputUtility.isMouseLeftClicked());
		assertFalse(InputUtility.isMouseRightClicked());
		assertFalse(InputUtility.getKeyTriggered(KeyCode.SPACE));
	}
}
