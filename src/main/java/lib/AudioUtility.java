package lib;

import javafx.scene.media.AudioClip;

public class AudioUtility {

	private static final String shoot = "se/shoot.wav";
	private static final String collect = "se/collect.wav";
	private static AudioClip sound_shoot;
	private static AudioClip sound_collect;
	static {
		loadResource();
	}

	public static void loadResource() {
		sound_shoot=new AudioClip(ClassLoader.getSystemResource(shoot).toString());
		sound_collect=new AudioClip(ClassLoader.getSystemResource(collect).toString());
	}

	public static void playSound(String identifier) {
		if(identifier=="shoot"){
			sound_shoot.play();
		}
		else if(identifier=="collect"){
			sound_collect.play();
		}
	}
}
